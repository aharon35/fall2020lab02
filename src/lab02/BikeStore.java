//Aharon Moryoussef
//1732787
package lab02;
import lab02.Bicycle;

public class BikeStore {
public static void main (String[]args) {
 Bicycle bikeArray[] = new Bicycle[4];
 bikeArray[0] = new Bicycle("CCM",21,40);
 bikeArray[1] = new Bicycle("SuperCycle",21,30);
 bikeArray[2] = new Bicycle("Canondale",27,55);
 bikeArray[3] = new Bicycle("BMX",1,25);
 
 for(int i = 0; i < bikeArray.length; i++) {
	 System.out.println(bikeArray[i].toString());
 }
 
}

}


