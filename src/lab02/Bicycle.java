//Aharon Moryoussef
//1732787
package lab02;

public class Bicycle {
String manufacturers;
int numberGears;
double maxSpeed;
public Bicycle(String manufacturers,int numberGears,double maxSpeed) {
	this.manufacturers = manufacturers;
	this.numberGears = numberGears;
	this.maxSpeed = maxSpeed;
}
public String toString() {
	return("Manufacturers: " + this.manufacturers + "\n" + 
			"Number of Gears: " + this.numberGears + "\n" + 
			"Maximum Speed: " + this.maxSpeed + "\n");
}

}


